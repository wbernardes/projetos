﻿using GenericSerializer.Factory;
using System;
using System.Collections.Generic;

namespace Serializer
{
    class Program
    {
        static void Main(string[] args)
        {
            string json = @"{'widget': { 'debug': 'on', 'window': { 'title': 'Sample Konfabulator Widget', 'name': 'main_window', 'width': 500, 'height': 500 }, 'image': { 'src': 'Images/Sun.png', 'name': 'sun1', 'hOffset': 250, 'vOffset': 250, 'alignment': 'center' }, 'text': { 'data': 'Click Here', 'size': 36, 'style': 'bold', 'name': 'text1', 'hOffset': 250, 'vOffset': 100, 'alignment': 'center', 'onMouseUp': 'sun1.opacity = (sun1.opacity / 100) * 90;' } }}";
            Console.WriteLine("Json:");
            Console.WriteLine(json);

            Console.WriteLine("Json Deserializado (Widget Title):");
            var x = SerializerFactory.Create(SerializerType.JSON);
            var entity = x.Deserialize<RootObject>(json);
            Console.WriteLine(entity.widget.window.title.ToString());

            x = SerializerFactory.Create(SerializerType.XML);
            var xml = x.Serialize<RootObject>(entity);
            Console.WriteLine("XML:");
            Console.WriteLine(xml);

            Console.WriteLine("Xml Deserializado (Widget Title):");
            entity = x.Deserialize<RootObject>(xml);
            Console.WriteLine(entity.widget.window.title.ToString());

            Console.ReadLine();
        }
    }

    public class Window
    {
        public string title { get; set; }
        public string name { get; set; }
        public int width { get; set; }
        public int height { get; set; }
    }

    public class Image
    {
        public string src { get; set; }
        public string name { get; set; }
        public int hOffset { get; set; }
        public int vOffset { get; set; }
        public string alignment { get; set; }
    }

    public class Text
    {
        public string data { get; set; }
        public int size { get; set; }
        public string style { get; set; }
        public string name { get; set; }
        public int hOffset { get; set; }
        public int vOffset { get; set; }
        public string alignment { get; set; }
        public string onMouseUp { get; set; }
    }

    public class Widget
    {
        public string debug { get; set; }
        public Window window { get; set; }
        public Image image { get; set; }
        public Text text { get; set; }
    }

    public class RootObject
    {
        public Widget widget { get; set; }
    }
}
