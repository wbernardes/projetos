﻿using GenericSerializer.Concrete;
using GenericSerializer.Contracts;
using System;

namespace GenericSerializer.Factory
{
    public static class SerializerFactory
    {
        private static Type type;
        public static ISerializer Create(SerializerType name)
        {
            switch (name)
            {
                case SerializerType.XML:
                    type = typeof(XmlCustomSerializer);
                    break;
                default:
                    type = typeof(JsonSerializer);
                    break;
            }
            return (ISerializer)Activator.CreateInstance(type);
        }
    }
}
