﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GenericSerializer.Factory
{
    public enum SerializerType
    {
        XML,
        JSON
    }
}
