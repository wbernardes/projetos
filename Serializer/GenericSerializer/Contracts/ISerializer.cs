﻿
namespace GenericSerializer.Contracts
{
    public interface ISerializer
    {
        string Serialize<T>(T entity);
        T Deserialize<T>(string text);
    }
}
