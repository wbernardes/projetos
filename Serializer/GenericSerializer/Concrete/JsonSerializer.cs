﻿using GenericSerializer.Contracts;
using Newtonsoft.Json;

namespace GenericSerializer.Concrete
{
    class JsonSerializer : ISerializer
    {
        public T Deserialize<T>(string jsonString)
        {
            try
            {
                return JsonConvert.DeserializeObject<T>(jsonString);
            }
            catch
            {
                throw;
            }
        }

        public string Serialize<T>(T entity)
        {
            try
            {
                return JsonConvert.SerializeObject(entity);
            }
            catch
            {
                throw;
            }
        }
    }
}
