﻿using GenericSerializer.Contracts;
using System.IO;
using System.Xml.Serialization;

namespace GenericSerializer.Concrete
{
    public class XmlCustomSerializer : ISerializer
    {
        public T Deserialize<T>(string xmlAsString)
        {
            try
            {
                using (var stream = new StringReader(xmlAsString))
                {
                    var serializer = new XmlSerializer(typeof(T));
                    return (T)serializer.Deserialize(stream);
                }
            }
            catch
            {
                throw;
            }
        }

        public string Serialize<T>(T entity)
        {
            try
            {
                using (var writer = new StringWriter())
                {
                    var serializer = new XmlSerializer(typeof(T));
                    serializer.Serialize(writer, entity);
                    return writer.ToString();
                }
            }
            catch
            {
                throw;
            }
        }
    }
}
